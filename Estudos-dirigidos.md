# **Propostas de estudos dirigidos** #
**Aqui proponho estudos nas áreas de ciências exatas**

# Uma das áreas abordadas é a computação. #

Uma vez que esta área tem relação direta com outras ciências, como
a matemática, física.

Pode-se dizer ainda ter relação com ciências relacionadas 
a essas citadas porém em níveis maiores de abstração. 
Como eletrônica, telecomunicações e a propria computação em si.


### Eletrostática ###

Pode-se entender por eletrostática:
 Área da física que se ocupa do estudo da energia em forma de eletricidade
---
### **Eletricidade** ###

No estudo e aplicação da eletricidade, vemos duas formas para a energia.
A energia elétrica possui algumas características fundamentais.

[**Eletricidade**](Eletricidade/README.md)

* **Tensão**, medida em volts (V).
* **Corrente**, medida em ampéres(A).
* **Resistência**, medida em Ohm, esta possui o símpolo da letra grega ômega.


* **Circuito**
* **malhas**
---

### * **Eletrônica** ###

* O transistor
 Semicondutores.
* circuitos integrados.
Escala de integração.

## **Computação** ##

[**Computação**](Computação/README.md)
A maior parte destes estudos é direcionada para a computação.
Tem também aplicação prática em tecnologia da informação.

* Hardware, software, humanware.

* **Hardware**

 - Definição.
Entende-se por hardware os meios físicos de máquinas computacionais.

 - Conceito.
O hardware em si, é composto por todo o equipamento, eletroeletrônico.

* **software**

- Definição.
 - Conceito.
 - Programas.
 - Sistemas.

* **Humanware**

- Definição.
- Conceito.
 
---

## **Sistemas computacionais** ###

* **Sistemas Operacionais**.

[Breve introdução](Computação/Sistemas_operacionais/README.md).
O ambiente tipo ***UNIX**.
O ambiente de um sistema operacional, tem por meta a integração com o operador e o seu administrador.
O primeiro ambiente a ser criado e conhecido, foi o ambiente de texto.
Esse ambiente vem e segue o modelo do famoso *sistema unix**. Por este ter sido o primeiro a ser construído.
A maior parte dos sistemas da atualidade segue esse modelo, principalmente os sistemas unix-like"tipo UNIX".
Para maiores detalhes veja:[Ambiente](Computação/Ambiente-texto.md).

---

* **Redes**

 Aqui publico um estudo na área de redes, que pode trazer bons resultados, colaborando com o uso de tecnologia.
 Em um ambiente (LAN). desenovolvi a técnica a qual batizei de _cache de DNS progressivo*.
 Utilizando sistema GNU/Linux, no caso o Debian, para confecção de ambiente LAN.
 
[**Cache de dns progressivo**.](Computação/Redes/dns-progressivo.md)


Sobre wifi e transmissões de dados via rádio.

A frequÊncia tem algumas leis da física que definem vários fatores.

Uma dessa regras é a questão do comprimento da onda ser inversamente proporcional a sua frequência.

**Assim** Quanto maior a frequência, menor seu comprimento na forma de onda de energia.

Para sistemas de envio e recebimento de dados, a potência e latência de beacon **campo** do espectro de rádio, deve ser ajustada seguindo uma série de fatores.

O intervalo e ajuste do _raio_ de espaço físico que se deseja alcançar com o sinal.
>Essa regra tem várias aplicações.

Pode ser ajustado através da variável **beacon threshould** de alguns roteadores wifi, para isso utilizo o seguinte cálculo.


[**dimensionando o espaço de cobertura do sinal**](Computação/Redes/ajustes-wifi.md).

---

## **Programação** ##

Entendendo a atividade:

[**O desenvolvimento**](Computação/Programação/Entendendo.md)

O dimensionamento da informações e seu processamento.

Aqui faço uma breve explanação sobre o tema.

[**Dimensionando**](Computação/Programação/Dimensionamento.md)

## Conhecendo linguagens de alto nível ##

Algumas da linguagens mais usadas na atualidade, são construidas para ambientes de alto nível.

Dentre elas podemos citar e recomendar de acordo com a necessidade ou tarefa a ser executada, as sguintes:

[Linguagem python](Computação/Programação/python/README.md)

Esta linguagem é relativamente recente e possui muitas características que a estão tornando muito popular.

Ela possui muitos recursos e tem sido desenvolvida e ajustada a várias necessidades.


[Linguagem php](Computação/Programação/php/README.md)

Essa linguagem também recente, possui com uma de suas principais características, ter sido construída para desenvolvimento em ambientes web. 


[Linguagem perl](Computação/Programação/perl/README.md)

Essa já é um pouco mais antiga que as citadas acima, eu particularmente aprecio seu uso.

Ela teve seu início com a idéia de permitir criação de relatórios para dados mais rápidos de forma mais eficiente, pois seu interpretador é compilado e executado a nível de linguagem de máquina _assembly_.


**Ambiente de desenvolvimento** para sistemas, aplicativos e utilitários.
    Ambiente virtual.
    
Esse ambiente permite flexibilidade em testes e deseonvolvimento
sem interferir no sistema diretamente.
Aqui cito exemplos práticos de construção de ambientes virtuais.

[_construindo um ambiente virtual_](Computação/Programação/Ambientes_virtuais.md).

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)

