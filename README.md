# Aqui temos uma série de estudos direcionados a tecnologia #
    A proposta desse trabalho é difundir estudos e pesquisas que desenvolvo como professor e estudante de várias áreas da tecnologia.
    
# [**Índice dos estudos e propostas de pesquisa**](Estudos-dirigidos.md) #
    
> Aqui temos uma lista de opções a nível de redes sociais, para partilharem cultura e vários recursos.
> Atuando dentro da proposta e da visão de comunicação e uso da tecnologia, respeitando as liberdades essenciais, temos os ideais do movimento de sofwtare livre.

**Para saber mais sobre os direitos e a liberdade dos usuários e desenvolvedores de tecnologia, visite:**

### * [F**undação do software livre**](http://www.fsf.org) ###

A participação dos usuários de tecnologia de forma livre e consciente, permite a evolução e troca entre diferentes culturas, permitindo um sincretismo que por sua vez ajuda a evolução de todos, com uma visão de sociedade forte, livre e solidária.

### * [Lista de redes sociais livres](redes_sociais_livres.md) ###

*Bem vindo a grato pela participação*.

Espero que gostem e aproveitem nossas propostas.

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
