# A computação #
A computação já era prevista por no meio da idade industrial.
Alguns pensadores desse tempo já imaginavam, contruir máquinas capazes de executar tarefas humanas através de um conjunto de instruções "simples".

eu sinto o impacto da tecnologia aplicada ao modo de vida do homem moderno e me preocupo com o uso disso como forma de direcionamento e controle sobre culturas e me sinto capaz de tentar como professor traer alguma consciência dobre isso tanto nos simples usuários de tecnologia como nos responsáveis por sua construção.

hoje em dia como estudante e professor eu tento despertar além do interesse e conhecimento técnico a responsabilidade sobre o uso do conhecimento trazendo o respeito as diferenças culturais e sociais naqueles que ensino e tenho contato profissional.

recentemente construí três software packages e adotei a GPL3 como licensa para sua distribuíção, são simples para aplicação como estudos e também tem alguma funcionalidade como ajuda em anotações, um deles adicionei quarenta textos que uso em um curso que dou sobre computação.

tentei traduzir esses textos para inglês espanhol e meu idioma nativo português, todos se encontram em repositórios git de forma livre, não tive a intenção de obter retorno financeiro com isso mas sim colaborar com o pouco que sei para a educação de forma geral.


Esse software contendo os texto tem uma interface em modo texto e dá a opção ao usuário de ler os textos e também ouví los, fiz uma iso bootável para construção de um pen drive usando o debian bullseye e adicionando esse software.
O uso é bem simples e a idéia foi também auxiliar deficientes visuais como minha mãe.


**Podemos afirmar**:
Juntamente com várias ciências da atualidade a computação vem evoluindo grandemente.

Muitas ciências auxiliares vem sendo desenvolvidas e permitem, uma melhor integração entre ambientes de 
tecnologias diversas.

**Conhecendo sistemas operacionais**.

[Introdução](Sistemas_operacionais/README.md).

---

**Operação do sistema**.
[Ambiente](Ambiente-texto.md).

---

**Redes**
[Ajustes wifi](Redes/ajustes-wifi.md).

[DNS progressivo](Redes/dns-progressivo.md).

---

**Programação**.

[Conecendo o contexto](Programação/Entendendo.md).

[Criando ambientes virtuais](Programação/Ambientes_virtuais.md).

## Conhecendo linguagens de alto nível ##

Algumas da linguagens mais usadas na atualidade, são construidas para ambientes de alto nível.

Dentre elas podemos citar e recomendar de acordo com a necessidade ou tarefa a ser executada, as sguintes:

[Linguagem python](Programação/python/README.md)

Esta linguagem é relativamente recente e possui muitas características que a estão tornando muito popular.

Ela possui muitos recursos e tem sido desenvolvida e ajustada a várias necessidades.


[Linguagem php](Programação/php/README.md)

Essa linguagem também recente, possui com uma de suas principais características, ter sido construída para desenvolvimento em ambientes web.


[Linguagem perl](Programação/perl/README.md)

Essa já é um pouco mais antiga que as citadas acima, eu particularmente aprecio seu uso.

Ela teve seu início com a idéia de permitir criação de relatórios para dados mais rápidos de forma mais eficiente, pois seu interpretador é compilado e executado a nível de linguagem de máquina _assembly_.


# **Arquiteturas de aplicações** #

Hoje em dia temos várias arquiteturas para desenvolviemnto, cada uma com um tipo de aplicação distinta.

Para aplicações web, a mais difundida é a arquitetura REST.

Essa arquitetura, abrange muitos recursos sistemas e linguagens permitindo a integração entre elas.

[**Conceito:**](REST.md)

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
