## O ambiente de texto ##
O dispositivo de comunicação serial.

O dispositivo virtual terminal.
* Dispositivo de entrada padrão(STDIN).
* Dispositivo de saída padrão(STDOUT).

O ambiente de texto de um sistema operacional.

Ambientes **UNIX Like/tipo unix**.
tty.
getty.

A interface provida pelo interpretador de comandos.
Ambiente, entrada e saída (E/S),(I/O).
Variáveis.


# A estrutura/árvore. #
**Trabalhando com o sistema de arquivos**.


**FHS**
Estrutura padrão do modelo tipo unix/unix like.

**Arquivos e diretórios**.

Visualizando:
**ls** (a instrução).
Obtendo informações:
**file** (a instrução).

type (a instrução).

* Copiando.
**cp** (a instrução).
* Movendo/renomeando.
**mv** (a instrução).

* removendo/apagando.
**rm** ((a instrução).
* Alterando permissões.
**chmod** (a instrução).

* Alterando propriedades.
**chown** (a instrução).

## Atributos extendidos ##
Esses atributos variam de acordo com uma série de fatores relacionados ao sistema de arquivosm em uso.

O método e modo de montagem do sistema de arquivos são importantes, para estas funcionalidades.

* Alterando atributos.
**chattr**(a instrução).

**lsattr**(a instrução).

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
