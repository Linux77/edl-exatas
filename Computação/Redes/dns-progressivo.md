## **DNS progressivo, cache progressivo** ##
Considerações:
Gosto de contato direto, tipo chat para conversar, não sou muito adepto a conteúdos estáticos.

**Proposta de experiência.**
* Sistema utilizado.

_Debian GNU/Linux_.

Creio ser possível tal experimento com quase todas as distribuições Linux.

A versão do debian que estou usando nesses experimentos é o testing.

posso acompanhar a descrever a impantação de um método que batizei como _cache de dns progressivo.*

Em pequena escala, utilizei o dnsmasq, em duas vms(máquinas virtuais).
Usando recursos como a libvirt, qemu e kvm. 

Criando dois guests(convidados) em um host(hospedeiro).

Os guests usados foram configurados usando o mesmo sistema do host.

Nesse caso  configurando o dnsmasq(sistema de servidor de DNS e cache de DNS de pequeno porte), nos guestse também os serviços de servidor ntp, para a sincronia com servidores de tempo na rede, a partir guests.

em cada guest aponto a resolução de nomes no servidor dnsmasq  para um par de servidores dns públicos, a consulta do dnsmasq a nível de server/cache.
**o sistema progressivo.**.

com o dimensionamento do tamanho do cache do servidor dnsmasq nos guests, progredindo de um valor que se inicia, na quantidade de memória do host(hospedeiro) adicionada ao valor da memória do primeiro guest, no caso cache-ns1, e o tamanho no segundo guest server/cache-ns2, com o valor da soma anterior mais a quantidade de memória dele, como: host dnsmasq = (mem), guest1 = (mem host) + (mem guest1), guest2 = (mem host) + (mem guest1) + (mem guest2).

Isso para o dns ajudou muito em vários fatores.
Sobre o experimento posso afirmar que o **recurso de alta disponibilidade**, usado em outros protocolos, se mostrou através de tal configuração, no dns na rede local, os clientes ficaram com um desempenho que me _surpreendeu.*

Outro fator interessante é que direcionei a consulta de dns do host(hospedeiro), para os servidores dnsmasq nos guests.

**Utilização em rede local(LAN)**.

Recomendo a configuração de rede dos guests em modo bridge com a interface também bridge do host, assim disponibilizando o serviço em toda a lan.

Outro fator percebido com o experimento, foi o número de requisições simultãneas suportado pelos aplicativos; dentro dos ambiente dos clientes da rede, por exemplo os aplicativos parecem ter adquirido maior independência no ambiente de rede.
