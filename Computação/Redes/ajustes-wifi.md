## Otimização de dados via rádio(Wifi) ##
# O intervalo de envio/recebimento do sinal. #
Sabendo que a eletrônica, em conjunto a eletricidade, parte do estudo da física; nos proporciona a condição de emitir e receber sinais de rádio. 
Podemos assim controlaro tempo de intervalo em um sistema de envio/recebimento.

Unindo esse conhecimento a computação, para alpicação em meios de
prática na informática. Nos cabe o ajuste de tempo entre o envio e recebiemnto de sinais de rádio as transmissões de datagramas em um ambiente como wifi.

Usando como base para o cálculo uma constante(d), para a distância em metros, adoto aqui o valor de 5 metros.
* Em um caso de **2,4Ghz**, o valor de tempo de transmissão do sinal com a constante (d) será de 3 x 10^⁻6.
* Em caso de **5 Ghz**, o valor de tempo de transmissão do sinal com a constante(d) será de 1,6 x 10^-6.

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
