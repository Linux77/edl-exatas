# Entendendo o dimensionamento de dados #

===
O que vem a ser o dimensionamento de dados.

* *Comprendendo a importância**.
    
    - Especificando o tamanho do sistema de arquivos.
    O sistema de arquivos deve ser planejado para cobrir as necessidades.
    
    O tipo de sistema operacional e suporte ao sistema de arquivos que suporte a quantidade de dados a serem armazenados.
    - Calculando.
    Estimativa de tamanho de dados.
    - Velocidade e número de requisições aos dados.
    
    - Sistema de QUOTAS.
    
    - Mídias necessárias.
    
## Limites ##

*   **limitações físicas**.
    - Espaço de alocação em mídia.
    - Quantidade de memória.
    
*   **limitações do sistema**.
    _Espaço de alocação em sistema de arquivos_.
    _Espaço de alocação de memória_.
---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
