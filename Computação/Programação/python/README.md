# Ambiente de desenvolvimento #

* python.

Linguagem de programação em alto nível.

Uso de interpretador.

Compilação e linkagem do interpretador e componentes e alto nível.

GCC/G++.

Bibliotecas e API's.


## Desenvolvimento web ##

- **MVC**.

**Django**.

Configuração do ambiente de desenvolvimento/produção.

Debian GNU/Linux.

Apache:

Nginx:

Integração usando **gunicorn**.

Configuração:

_Serviços_.

systemd.

/etc/systemd/system/gunicorn.socket

/etc/systemd/system/gunicorn.service
