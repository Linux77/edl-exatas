## Entendendo o cenário. ##
A programação e o desenvolviemtno.
**Programação**.
* codificação.
* Compreendendo a linguagem.
* A estrutura.
    Programação seguindo ordens de eventos(sequências).
    Estrutura de procedimentos.
    Instruções.
    Parâmetros.
    Argumentos.
**O modelo de estrutura**.
Os procedimentos são usados para dividir o código e podem ser invocados de forma aleatória.
Essa técnica é conhecida como programação _estruturada_.
Esse tipo e técnica muito comum em muitas linguagens pode ser entendido como um estágio na evolução da tecnologia e da própria programação. Essa prática torna o código mais complexo para ser lido e compreendido pelos programadores que desconhecem as finalidades e própria lógica das estruturas de execução desses ditos procedimentos.
Esse momento da evolução tecnológica deu origem a um termo muito usado _programação estruturada_.
**A padronização**.
A linguagem mais difundida para desenvolvimento a nível de componentes de sistemas operacionais por muito temmpo e até os tempos atuais  é a linguagem C, porém devido ao variado modo de se estruturar utilitários e outros tipos de aplicações que necessitem interpretação e execução em baixo nível onde o próprio harware seja capaz de produzir resultados imediatos, onde o cófigo final da aplicação será em bytecode, conjunto de instruções em nível de execução praticamente direta pelo equipamento computacional.fora realizada um padronização juntamente com implementação de novas técnicas e métodos para a codificação estruturação e organização lógica das aplicações em C.
Em um momento não muito distante de nossos tempos 
surgiu o modelo orientado a objetos e a normatização para um ambiente conhecido como C++ o que veio a ser por muitos adotado para construção de muitas partes e componentes dos sistemas da atualidade.
Uma leitura muito importante para os desenvolvendores é exatamente as regras e normas que foram e são estabelecidas no C++.
Particularmente adoto guias para tais regras, mas acredito também ser muito importante um guia de refência da linguagem propriamente dita.
Dentro dessa nova visão e método o que será visto como OOP Object Oriented Programming _programação orientada a objetos_.
Teremos a estruturação e um conjuntos de biblioteças elementares, o qual o estudo se torna indispensável aqueles que realmente desejam se tornar desenvolvedores.
O modelo de bibliotecas padrão _standard template library **STL**.
A STL é composta por uma série de elementos e objetos de código fundamentais assim como espaços de ambiente _namespaces_ com os quais o desenvolvedor poderá cosntruir seus próprios objetos e elementos para seus programas, utilitários e até mesmo componentes para o sistema operacional e computacional.

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
