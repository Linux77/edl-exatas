## Criando o ambiente ##
**O Ambiente de programação.**

Uma das primeiras tarefas que um programador
deve compreender, é a construção do ambiente.
Para isso presume-se que ele já possua conhecimentos sobre
o funcionamento básico de um sistema.
Os componentes de um sistemaoperacional.

* Utilitários.
Esses são programas para gerenciamento de
recursos do sistema, configurações e manutenção.
Ficam aqui entendidos programas que realizam ajustes
controle e outras atividades relacionadas ao próprio sistema.

* Aplicativos.
Esse por sua vez tornam o sistema aplicável a atividades
humanas, eles constrõem comunicação entre outros componentes
do sistema e possuem uma forma de comunicação E/S de informações ao
usuário(operador) do sistema.

* Bibliotecas.
Estas são encarregadas de trazer códigos diversos comuns aos aplicativos e utilitários, no que diz respeito a atividades técnicas.
As bibliotecas estão divididas de várias formas. Pode-se entender que
elas vão construir um ambiente de comunicação permitindo as mais diversas linguagens de programação ter acesso aos recursos do sistema.

## Um ambiente isolado ##

**Entendendo o ambiente**

Um ambiente pode ser construído de forma isolada, dos recursos
do sistema, assim o programador tem como adicionar e remover componentes sem por a estrutura e funcionamento do sistema em
constantes modificações e testes não tão seguros.

* PERL.

O perl possui uma série de módulos e também conta com o recurso de
construção de um ambiente isolado.
Esse constrói um ambiente __virtual__ para o desenvolvimento.

Para isso podemos começar com um usuário sem _privilégios_:

Nesse exemplo é usado o debian/GNU Linux com o perl padrão da instalação.

#~ cpan install App::Virtualenv

Essa instrução usa o instalador de módulos cpan(nativo do perl) para instalar o módulo de construção de ambientes virtuais.

Após o módulo instalado podemos criar nosso primeiro ambiente virtual.

#~ perl -MApp::Virtualenv -erun -- ambiente

Isso irá criar um subdiretório noo diretório atual contendo nosso ambiente.

para ativá-lo usamos:

#~ source ambiente/bin/activate

para sair do ambiente virtual usamos a palavra chave _deactivate_.

--- 

* Python.

O python possui uma série de módulos incluindo um que permite a construção desse ambiente __virtual__ de forma segura e isolada dos
demais componentes do sistema. Permitindo assim liberdade ao programador.

Para isso precisamos do módulo virtualenv da linguagem. Ele pode ser instalado de várias formas de acordo com o sistema.

Em ambiente linux o módulo se encontra nas próprias listas de pacotes.

Usando o róprio instalador de módulos do python o "pip" pode-se usar a seguinte sintaxe:

#~pip install virtualenv

Assim teremos a instrução virtualenv disponível. Para criarmos o ambiente virtual, usamos:

virtualenv ambiente

Feito isso será criado o ambiente no diretório respectivo. Para ativá-lo usamos:

#~source ambiente/bin/activate

Para sairmos do ambiente virtual usamos a palavra chave _deactivate_.

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
