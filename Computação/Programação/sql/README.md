# Usando recursos do MySQL/Mariadb #

Structured Query Language.

_Recursos fundamentais_.


* INSERT/UPDATE.

* DELETE.

**Construção de tabelas**.

* CREATE 

* ALTER

- DROP

**Recursos avançados**
- Stored procedures.
>Funções personalizadas armazenadas no banco de dados.
>Argumentos/Parâmetros para funções.

Delimitadores.
> // &&

- Views.

- Triggers.
