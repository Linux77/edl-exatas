# Comunicação entre aplicativos #

**Aplicações a nível de sistemas**.

>Esta se aplicam a aplicativos, utilitários e até mesmo componentes do sistema operacional.

Estes métodos atuam a nível de sistemas operacionais seguindo o modelo POSIX.

Dentro dessa categoria estão inclusos praticamente todos os sistemas da atualidade.

Eles pode ser entitulados UNIX like ou NON_UNIX like.

Assim seriam tidos como sistemas tipo **UNIX**, e sistemas não tipo **UNIX**.

## RPC/IPC ##

**Remote procedure call**.

Chamada de procedimento remota.

>Esse tipo de requisição é feita através de um mapeamento de portas onde as áreas do usuário/kernel e a do operador(cliente do ambiente do sistema), se comunicam, relizando conexões através de uma rede loca(interna) no sistema operacional do servidor, na maior parte das vezes, também conhecida e denomida de tarefas com execução server side.
>Embora essas tarefas pareçam ser executadas inteiramente pelo sistema do servidor como um bloco de instruções, elas podem realizar várias conexões na rede interna do servidor ou em um ambiente de rede da rede de serviçoes.

**Internal proccess comunication**.

Comunicação interna entre processos.

>Essa é a forma de um determinado software se comunicar internamente com outros de seus prórpios componentes, em caso de aplicações em baixo nível.

## REST - Restful ##


* **Aplicações web**.

As aplicações web muitas vezes são independentes umas das outras, cabendo a cada uma um serviço ou função diferente.

Com isso temos a necessidade de criar meios de comunicação entre aplicações baseando-se nos dados que estas irão manipular.

O modelo adotado para esta comunicação muito conhecido com REST, na realidade é um ambiente onde temos um conjunto de classes e objetos, bem como recursos para diversas linguagens distintas. O nome REST é um acrônimo de Representational State Transfer, o que é entendido como um estilo de arquitetura. Este utiliza um protocolo muitas vezes estático para esta comunicação entre aplicações.

Na maior parte dos casos é usado o protocolo HTTP.

O rpocedimento e sua estrutura:

>Na construção de aplicações com essa arquitetura temos quatro formas de manipular as informações e seu envio e recebimento.

**GET**:

>Este é o método de recepção, recebimento/consulta de dados.

**POST**:

>Este é o método de criação/envio, criação do bloco(buffer) de dados.

**PUT**:

>Este é o o método de reenvio para atualização, update, para os dados.

**DELETE**:

>Este é o método para se apagar as informações.
>Também pode ser entendido como chamada de execução para um procedimento no servidor.

Podemos entender os dados ou informações como sendo as variáveis, objetos, propriedades, e métodos, ou ainda as informações em sua forma primitiva.

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
