## Construindo um ambiente MVC catalyst perl ##

**Pacotes a serem instalados**.

Para suporte ao MVC catalyst em sistemas debian:

**Ambiente integrado com nginx**.

- instalar nginx

apt install nginx

_Módulos necessários para o nginx_.

apt install libnginx-mod-http-perl

O nginx será o responsável pela publicação do serviço.
> Para isso o arquivo de configuração do virtual host usado pelo serviço tem de atender algumas especificações.

**Componentes e catalyst MVC**.

apt install libcatalyst-devel-perl

apt install libcatalyst-plugin-static-simple-perl

apt install libcatalyst-plugin-configloader-perl

apt install libcatalyst-action-renderview-perl

---

No diretório do projeto catalyst, subir o serviço:

Ex:

**old_black**.

cd /var/www/old_black

script/old_black_fastcgi.pl -l 127.0.0.1:3000

Desta forma o servidor http, no caso o nginx poderá fazer o encaminhamento das requisições para o aplicativo perl.
