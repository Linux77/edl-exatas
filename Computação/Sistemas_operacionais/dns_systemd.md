# /conhecendo os recursos de bind/dns #

**Novos recursos**.
O _systemd_.

O novo mecanismo de administração ainda não explorado profundamente por muitos usuários de ambientes _Linux_.

O antigo controle de execução e níveis de funcionamento do sistema, o sysvinit, o qual trazia uma tabela para os níveis de execução, este era um pouco limitado, sabendo-se que tínhamos uma tabela e os ambientes de execução seriam executados dentro de um único ambiente virtual.

Com o surgimento do systemd, o sistema constrói ambientes próprios com ou sem relações entre si, o que permite ao sistema um controle descentralizado, porém mantendo algumas características do antigo modelo.

Um recurso que descobri mas provavelmente já é do conhecimento de muitos é o serviço de bind/dns local através do proprio sistema, sem a necessidade de um aplicativo ou daemon externo para a aceleração de consultas e cache de dns.

No arquivo de configuração da resolução de nomes, antes estático como recurso da própria glibc, agora permite um daemon local integrado com o sistema de forma robusta.
No arquivo de configuração padrão do sistema de resolução de nomes no novo formato, que pode ser encontrado em /etc/systemd/resolved.conf, temos a opção DNSStubListener, essa opção e também serviço, constrói um daemon local no endereço 127.0.0.53, que pode ser apontado por um arquivo resolv.conf estático no diretório /etc, /etc/resolv.conf, assim o cache e serviço de consultas por dns/bind, fica isolado no próprio host local, em testes aqui essa técnica aumentou consideravelmente a estabilidade, velocidade e segurança do Desktop.

Creio que pelo texto acima se torna fácil a implementação de tal experiência para havaliação e uso desse recurso.

**A.S.L**
_Leonardo de Araújo Lima_.
