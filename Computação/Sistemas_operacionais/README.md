## Sistemas operacionais ##
**O sistema operacional**.

Entende-se por _sistema_, o coletivo de programas de computador.
Entende-se por sistema operacional, um conjunto de
programas de computador cuja principal função é
permitir entrada e saída de dados a um nível prático para
as tarefas do ser humano de forma geral.

Em um determinado momento da confecção dos aparatos tecnológicos
foi estabelecido, um conjunto de normatizações, muitas destas para
permitir compatibilidade entre os diversos modelos e métodos de
armazenamento de dados.

O modelo para tal, foi o modelo de interface de sistema operacional
portável. **POSIX** _portable operating systema interface_.

Um sistema é um conjunto de vários tipos de programas _codificados de várias formas_.

Os programas de computador inicialmente podem ser compreendidos de acordo com a finalidade e a linguágem adequada a esta finalidade.

---

**Podemos Analisar da seguinte forma**.

* Alto nível.

**Humaware**.

Atividades relacionadas a abstração técnica e sim aplicação em dados
relacionados a atividades humanas. Esse termo alto nível aplica-se sob a compreensão de quanto mais próximo ao equipamento mais distante do _operador_ humano, e suas aplicações.

- Aplicativos

- Utilitários

- Bibliotecas

---

* Baixo nível.

**Hardware**.

Esse tipo de prática em programação se destina a confecção de controles para o equipamento, ou ainda comunicação entre dispositivos e aplicações mais próximas do ser humano.
Para isso são usados na maior parte das vezes, códigos em linguagens que o proprio hardware _equipamento eletrônico_ seja capaz de interpretar e executar.

**Software**.

Aplicações para construção de suporte a subsistemas dentro do sistema operacional.
Aplicações em nível de comunicação com controladores de hardware e outras camadas entre o ser humano e a máquina propriamente dita.

- TSR's

Terminate and stay resident, esse termo é aplicado a programas de vários tipos e funções que permanecem alocados na memória mesmo após relaizarem sua tarefa.
O caso mais conhecido desse tipo de programas, são os daemos, serviços decontroladores"servidores" de algum paotocolo de rede.

- Driver's

Os drivers como o próprio nome sugere, são "pilotos" controladores para um determinado equipamento.
Existem várias formas para esse controle e muitos dividindo-se em várias etapas e camadas.

- Daemon's

Esse por sua vez, são encarregados por permitirem as conexões entre serviços locais ou remotos.
Muitos formatos de ambientes de rede, dependem deste tipo de programas para relizarem os dois lados das conekões. O server side _lado do servidor_ e o client side _lado do cliente_.

- Bibliotecas

Em muitas situações o desenvolvedor ou programador teria de realizar repetições de códigos e ou ainda reescrever códigos que outros programadores já escreveram. Para solucionar esses problemas e em alguns casos separar modelos de execução dentro do sistema, os programas contam com outros programas externos, nesse caso são contrídas bibliotecas, contendo recursos que outros programas utilizarão.

- Motores

- API's

---

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)
